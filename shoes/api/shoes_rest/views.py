from django.shortcuts import render
from .models import Shoe, BinVO
from common.json import ModelEncoder
from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
import json
# Create your views here.
class BinVODetailEncoder(ModelEncoder):
    model=BinVO
    properties = ["closet_name", "import_href"]
class ShoeListEncoder(ModelEncoder):
    model=Shoe
    properties = [
        "name",
        "picture_url",
        "id",
    ]
    def get_extra_data(self, o):
        return {"bins": o.bin.closet_name}
class ShoeDetailEncoder(ModelEncoder):
    model=Shoe
    properties = [
        "manufacturer",
        "name",
        "color",
        "picture_url",
        "bin",
    ]
    encoders = {
        "bin": BinVODetailEncoder(),
    }



@require_http_methods(["GET","POST"])
def api_list_shoes(request, bin_vo_id=None):
    if request.method=="GET":
        shoes = Shoe.objects.all()
        return JsonResponse(
            {"shoes": shoes},
            encoder=ShoeListEncoder,
        )
    else:
        content = json.loads(request.body)
        print("Bin vo ID: ",bin_vo_id)
        try:
            bin_href = content["bin"]
            print("bin_href: ",bin_href)
            bin = BinVO.objects.get(import_href=bin_href)
            content["bin"]=bin
        except BinVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid bin id"},
                status=400,
            )
        shoe = Shoe.objects.create(**content)
        return JsonResponse(
            shoe,
            encoder=ShoeDetailEncoder,
            safe=False,
        )

@require_http_methods(["GET","DELETE","PUT"])
def api_show_shoe(request, pk):
    if request.method=="GET":
        shoe = Shoe.objects.filter(id=pk)
        return JsonResponse(
            {"shoe": shoe},
            encoder=ShoeDetailEncoder,
        )
    elif request.method=="DELETE":
        try:
            shoe = Shoe.objects.get(id=pk)
            shoe.delete()
            return JsonResponse({"message": "Shoe has been deleted"})
        except Shoe.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})
    else:
        try:
            content = json.loads(request.body)
            if "bin" in content:
                content["bin"] = BinVO.objects.get(id=content["bin"])
            Shoe.objects.filter(id=pk).update(**content)
            shoe = Shoe.objects.get(id=pk)
            return JsonResponse(
                shoe,
                encoder=ShoeDetailEncoder,
                safe=False,
            )
        except Shoe.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})